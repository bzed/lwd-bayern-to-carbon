#!/usr/bin/env python3

import yaml
import time
import graphyte
import traceback
import sys
import requests
from datetime import datetime as dt

# CONFIG = {}

MEASUREMENT_MAP = {
    "HS2": "snow_height_2",
    "HS": "snow_height",
    "ff": "wind_speed",
    "dd": "wind_direction",
    "ffBoe": "wind_gust",
    "TL": "temperatur_luft",
    "TO": "temperatur_oberflaeche",
}

MEASUREMENT_FACTORS = {
    "ff": 3.6,
    "ffBoe": 3.6,
}

# GRAPHYTE_CARBON = None
# GRAPHYTE_TAGGED = None


def update_station(station_id, station_name):
    url = f"https://api-la-dok.bayern.de/public/weatherWeb/{station_id}"
    try:
        r = requests.get(url)
        station_data = r.json()
    except Exception as e:
        print(f"Failed to update {station_name}: {e}")
        return 0
    if not station_data:
        return 0
    current_data = station_data[-1]
    ts = current_data["TS"].replace(" ", "T")
    ts = f"{ts}+01"
    measurement_time = dt.fromisoformat(ts).timestamp()

    weather_data = {}
    for _old, _new in MEASUREMENT_MAP.items():
        if _old in current_data:
            factor = 1
            if _old in MEASUREMENT_FACTORS:
                factor = MEASUREMENT_FACTORS[_old]
            weather_data[_new] = current_data[_old] * factor
    print(
        "Updating {}/{}: {} ({})".format(
            station_id, station_name, str(weather_data), ts
        )
    )

    for _k, _v in weather_data.items():
        _v = float(_v)
        metric = f"{station_name}.{_k}.value"
        GRAPHYTE_CARBON.send(
            metric,
            _v,
            timestamp=measurement_time,
        )
        GRAPHYTE_TAGGED.send(
            _k,
            _v,
            timestamp=measurement_time,
            tags={"location": station_name, "measurement": _k, "source": "lwd_bayern"},
        )
    return measurement_time


def filter_carbon_name(name):
    name = str(name)
    name = (
        name.replace("/", "_")
        .replace("ö", "oe")
        .replace("ü", "ue")
        .replace("ä", "ae")
        .replace("Ä", "Ae")
        .replace("Ö", "Oe")
        .replace("Ü", "Ue")
        .replace("ß", "ss")
    )
    return name


def get_stations():
    url = "https://api-la-dok.bayern.de/public/weatherstations/all"
    r = requests.get(url)
    stations = r.json()
    stations = {i["stationId"]: filter_carbon_name(i["name"]) for i in stations}
    return stations


def main():
    keep_running = True

    while keep_running:
        last_update = 0.0

        try:
            stations = get_stations()
            for _id, _name in stations.items():
                last_update = max(update_station(_id, _name), last_update)
        except KeyboardInterrupt:
            print("Received Keyboard interrupt, exiting")
            keep_running = False
            break
        except Exception as e:
            print("Update of station list failed: {}".format(e))
            traceback.print_exc(limit=10, file=sys.stderr)

        sleep_time = (last_update + 11 * 60) - time.time()
        if sleep_time > 0:
            time.sleep(sleep_time)


if __name__ == "__main__":

    print("Starting lwd-bayern-to-carbon!")
    if len(sys.argv) < 2:
        print("Usage: {} <configfile>".format(sys.argv[0]))
        sys.exit(1)

    with open(sys.argv[1], "r") as config_file:
        CONFIG = yaml.safe_load(config_file)
        GRAPHYTE_CARBON = graphyte.Sender(**CONFIG["carbon"])
        tagged_config = CONFIG["carbon"]
        tagged_config["prefix"] = ""
        GRAPHYTE_TAGGED = graphyte.Sender(
            **tagged_config, tags={"source": "lwd_bayern"}
        )
    try:
        main()
    except KeyboardInterrupt:
        print("Received Keyboard interrupt, exiting")
